FROM golang:latest

# Set necessary environmet variables needed for building spamcheck's golang components
ENV GO111MODULE=on
ENV GOOS=linux
ENV GOARCH=amd64

# Update debian's distribution lists
RUN apt-get update -y

# Install general purpose utilities
RUN apt-get install -y apt-utils curl git jq netcat unzip wget

# Install grpcurl

# RUN go get github.com/fullstorydev/grpcurl/...
RUN go install github.com/fullstorydev/grpcurl/cmd/grpcurl@latest

# Install grpc-health-probe
RUN go install github.com/grpc-ecosystem/grpc-health-probe@v0.4.4

# Install packages needed to build spamcheck's ruby protobuf client
RUN apt-get install -y ruby rubygems

# We build and install a specific non-packaged version of python, here we install its dependencies
RUN apt-get install -y --no-install-recommends \
	build-essential \
	curl \
	libbz2-dev \
	libffi-dev \
	liblzma-dev \
	libncurses5-dev \
	libreadline-dev \
	libsqlite3-dev \
	libssl-dev \
	libxml2-dev \
	libxmlsec1-dev \
	llvm \
	make \
	tk-dev \
	wget \
	xz-utils \
	zlib1g-dev

# Get and install pyenv
RUN git clone --depth=1 https://github.com/pyenv/pyenv.git /root/.pyenv
ENV PYENV_ROOT="/root/.pyenv"
ENV PATH="${PYENV_ROOT}/shims:${PYENV_ROOT}/bin:${PATH}"

# Install and configure our desired python version
ENV PYTHON_VERSION=3.9.5
RUN pyenv install ${PYTHON_VERSION}
RUN pyenv global ${PYTHON_VERSION}

# Install tini
ENV TINI_VERSION v0.19.0
ADD https://github.com/krallin/tini/releases/download/${TINI_VERSION}/tini /
ADD https://github.com/krallin/tini/releases/download/${TINI_VERSION}/tini.sha256sum /
WORKDIR /
RUN sha256sum -c tini.sha256sum && chmod +x tini

# Move to working directory /build
WORKDIR /build

# Copy the code into the container
COPY . .

# Copy and download dependencies using go mod based on go.mod and go.sum
#RUN go mod download

# get and configure tensorflowlite libraries
COPY app/mlengine/amd64/libtensorflowlite_c.so /usr/local/lib
RUN export LIBRARY_PATH=$LIBRARY_PATH:/usr/local/lib
RUN export DYLD_LIBRARY_PATH=$DYLD_LIBRARY_PATH:/usr/local/lib
RUN ldconfig /usr/local/lib

RUN make build

# Export necessary port
EXPOSE 8001 8002 8080

# Command to run when starting the container
ENTRYPOINT ["/tini", "--"]

CMD ["bash", "/build/spam-classifier.sh"]
