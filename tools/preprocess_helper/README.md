# Preprocess_helper

Th `tools/preprocess_helper/***OS***/dist` directory contains preprocessing functionality in `preprocess.py` necessary for preparing GitLab issue data for serving to Spamcheck's machine learning model.

Please do NOT delete any files contained in the `preprocess_helper` directory.

For licensing information, please take a look at the `LICENSE.md` file at the base of the `preprocess_helper` directory.