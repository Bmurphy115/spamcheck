# coding: utf-8
prefix = 'ruby'
$LOAD_PATH.unshift(File.expand_path(File.join(prefix), __dir__))
require 'spamcheck/version'

Gem::Specification.new do |spec|
  spec.name          = "spamcheck"
  spec.version       = SpamCheck::VERSION
  spec.authors       = ["Ethan Urie"]
  spec.email         = ["eurie@gitlab.com"]

  spec.summary       = %q{Auto-generated gRPC client for SpamCheck}
  spec.description   = %q{Auto-generated gRPC client for SpamCheck.}
  spec.homepage      = "https://gitlab.com/gitlab-org/gl-security/engineering-and-research/automation-team/spam/spamcheck"
  spec.license       = "MIT"

  spec.files         = `git ls-files -z #{prefix}`.split("\x0").reject { |f| f.match(%r{^#{prefix}/(test|spec|features)/}) }
  spec.require_paths = [prefix]

  spec.add_dependency "grpc", "~> 1.0"
end
