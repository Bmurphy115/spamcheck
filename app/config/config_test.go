package config

import (
	"os"
	"testing"

	"github.com/stretchr/testify/assert"
)

var testGRPC = &GRPC{
	Port: "5050",
}

var testREST = &REST{
	ExternalPort: "5051",
}

var testGRPCNil = &GRPC{
	Port: "",
}

var testRESTNil = &REST{
	ExternalPort: "",
}

var testLogger = &Logger{
	Level:  "info",
	Output: "json",
	Format: "stderr",
}

var testMonitor = &Monitor{
	Address: "5053",
}

var testExtraAttributes = &ExtraAttributes{
	MonitorMode: "false",
}

var testFilter = &Filter{
	AllowList: map[string]string{"21": "testy/test"},
	DenyList:  map[string]string{},
}
var testFilterWithBoth = &Filter{
	AllowList: map[string]string{"21": "testy/test"},
	DenyList:  map[string]string{"26": "foo/bar"},
}

var testPreprocessor = &Preprocessor{
	SocketPath: "/foobar/preprocessor.sock",
}

var testModelAttributes = &ModelAttributes{
	ModelPath: "/foobar/model.tflite",
}

var testConfig = &Config{
	*testGRPC,
	*testREST,
	*testLogger,
	*testMonitor,
	*testExtraAttributes,
	*testFilter,
	*testPreprocessor,
	*testModelAttributes,
}

// https://yourbasic.org/golang/gotcha-why-nil-error-not-equal-nil/
// Assigns untyped nil
func AssignNil() (err error) {
	return
}

func TestLoadConfig(t *testing.T) {
	t.Run("Testing with correct file path", func(t *testing.T) {

		got := testConfig.LoadConfig("../../config/config.toml.example")

		want := AssignNil()

		assert.Equal(t, got, want)
	})

	t.Run("Testing with incorrect file path", func(t *testing.T) {
		err := testConfig.LoadConfig("../foo/bar.toml")

		assert.EqualErrorf(t, err, "reading config file :open ../foo/bar.toml: no such file or directory", "Should have an error reading non-existent file")
	})
}

func TestValidateConfig(t *testing.T) {
	t.Run("Testing with valid GRPC & REST ports", func(t *testing.T) {
		got := ValidateConfig(testConfig)

		want := AssignNil()

		assert.Equal(t, got, want)
	})

	t.Run("Testing with invalid GRPC port", func(t *testing.T) {
		testConfig := &Config{
			*testGRPCNil,
			*testREST,
			*testLogger,
			*testMonitor,
			*testExtraAttributes,
			*testFilter,
			*testPreprocessor,
			*testModelAttributes,
		}
		err := ValidateConfig(testConfig)
		assert.EqualErrorf(t, err, "invalid TCP port for gRPC server: ''", "Invalid gRPC port")
	})

	t.Run("Testing with invalid REST port succeeds", func(t *testing.T) {
		testConfig := &Config{
			*testGRPC,
			*testRESTNil,
			*testLogger,
			*testMonitor,
			*testExtraAttributes,
			*testFilter,
			*testPreprocessor,
			*testModelAttributes,
		}
		err := ValidateConfig(testConfig)
		assert.Nil(t, err, "Nil HTTP Port should be allowed")
	})

	t.Run("Testing with filled allowlist & denylist", func(t *testing.T) {
		testConfig := &Config{
			*testGRPC,
			*testREST,
			*testLogger,
			*testMonitor,
			*testExtraAttributes,
			*testFilterWithBoth,
			*testPreprocessor,
			*testModelAttributes,
		}
		err := ValidateConfig(testConfig)
		assert.EqualErrorf(t, err, "Both allowlist & denylist cannot have items at the same time", "Allowlist and Denylist should not both be set")
	})

}

func TestRunConfig(t *testing.T) {
	t.Run("Testing correct running configuration", func(t *testing.T) {
		os.Chdir("../../")
		got, errGot := RunConfigWithConfigFile("config/config.toml.example")

		want, errWant := testConfig, AssignNil()

		assert.Equal(t, got, want)
		assert.Equal(t, errGot, errWant)
	})
}
