package tracing

import(
	"gitlab.com/gitlab-org/labkit/log"
	"gitlab.com/gitlab-org/labkit/tracing"
)

func LoadTracing() {
	// Initialize LabKit tracing
	closer := tracing.Initialize(tracing.WithServiceName("spamcheck"))

	if closer == nil {
		log.WithError(nil).Warn("NoOp tracer set")
	}
}
