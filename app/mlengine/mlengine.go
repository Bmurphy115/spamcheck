package mlengine

/*
#cgo CFLAGS: -I ./include
//points to the right platform version of tflite libs
#cgo arm LDFLAGS: -L arm
#cgo darwin LDFLAGS: -L macosx
#cgo x86_64 LDFLAGS: -L x86_64
#cgo LDFLAGS: -ltensorflowlite_c
//Raspberry Pi needs to include libatomic when linking w/ tflite
#cgo arm LDFLAGS: -latomic
#include "tensorflow/lite/c/c_api.h"
#include <stdlib.h>
*/
import "C"
import (
	"bufio"
	"encoding/json"
	"fmt"
	"io"
	"net"
	"os"
	"path/filepath"
	"reflect"
	"regexp"
	"strconv"
	"strings"
	"unsafe"

	"github.com/golang/protobuf/jsonpb"
	"gitlab.com/gitlab-org/labkit/log"
	spamcheck "gitlab.com/gitlab-org/spamcheck/api/v1"
	config "gitlab.com/gitlab-org/spamcheck/app/config"
	timestamppb "google.golang.org/protobuf/types/known/timestamppb"
)

// TensorType is types of the tensor.
type TensorType int

// Tensors type
const (
	TfLiteNoType TensorType = iota
	TfLiteFloat32
)

// Tensor represents TensorFlow Lite Tensor.
type Tensor struct {
	tensor *C.TfLiteTensor
}

// Type() returns TensorType.
func (t *Tensor) Type() TensorType {
	return TensorType(C.TfLiteTensorType(t.tensor))
}

// ByteSize() returns byte size of the tensor.
func (t *Tensor) ByteSize() uint {
	return uint(C.TfLiteTensorByteSize(t.tensor))
}

// InterpreterOptions represents a TensorFlow Lite InterpreterOptions.
type InterpreterOptions struct {
	options *C.TfLiteInterpreterOptions
}

// JSONPayload define structs for our issue JSON payload
type jsonPayload struct {
	Entity entity `json:"entity"`
}

type entity struct {
	ID            uint                   `json:"id"`
	User          user                   `json:"user"`
	Title         string                 `json:"title"`
	Description   string                 `json:"description"`
	CreatedAt     *timestamppb.Timestamp `json:"created_at"`
	UpdatedAt     *timestamppb.Timestamp `json:"updated_at"`
	Action        spamcheck.Action       `json:"action"`
	UserInProject bool                   `json:"user_in_project"`
}

type user struct {
	Emails    []*spamcheck.User_Email `json:"emails"`
	Org       string                  `json:"org"`
	Username  string                  `json:"username"`
	CreatedAt *timestamppb.Timestamp  `json:"created_at"`
}

type userEmail struct {
	Email    string `json:"email"`
	Verified bool   `json:"verified"`
}

type MlEngine interface {
	GetSpamValue(issue *spamcheck.Issue) float32
}

type mlEngine struct {
	encoder *jsonpb.Marshaler
	cfg     *config.Config
}

func CreateMlEngine(cfg *config.Config) MlEngine {
	encoder := &jsonpb.Marshaler{OrigName: true}

	return &mlEngine{
		encoder: encoder,
		cfg:     cfg,
	}
}

// GetSpamValue Return float32 indicating how likely the issue is spam
func (m *mlEngine) GetSpamValue(issue *spamcheck.Issue) float32 {
	jsonPayload := m.createJSONPayload(issue)
	verdictValue := m.sendJSONPayload(jsonPayload)
	return verdictValue
}

func (m *mlEngine) reader(r io.Reader) [][]int32 {
	reader := bufio.NewReader(r)
	defer reader.Reset(r)

	buf := make([]byte, 4096)
	var formattedString string

	// get buffer length
	n, err := reader.Read(buf[:])
	if err != nil {
		return nil
	}

	// remove spaces and returns
	formattedString = strings.Join(strings.Fields(strings.TrimSpace(string(buf[0:n]))), " ")

	// use regex to match against integers in string; get each integer and append to an int array
	re := regexp.MustCompile("[0-9]+")
	var intArr []int32
	parts := strings.Split(formattedString, " ")
	for _, element := range parts {
		if len(re.FindAllString(element, -1)) != 0 {
			extractedInt := re.FindAllString(element, -1)[0]
			new_item, _ := strconv.ParseInt(extractedInt, 10, 32)
			final_item := int32(new_item)
			intArr = append(intArr, final_item)
		}
	}
	finalData := [][]int32{intArr[:]}
	return finalData
}

func (m *mlEngine) preprocessData(jsonValue []byte) ([][]int32, error) {
	conn, err := net.Dial("unix", m.cfg.Preprocessor.SocketPath)
	if err != nil {
		return nil, err
	}
	defer conn.Close()

	dataChan := make(chan [][]int32)

	var data [][]int32
	go func(outChan chan<- [][]int32) {
		outChan <- m.reader(conn)
	}(dataChan)

	writer := bufio.NewWriter(conn)
	if _, err := writer.Write(jsonValue); err != nil {
		return nil, err
	}
	writer.Flush()

	data = <-dataChan
	return data, nil
}

// CreateJSONPayload creates our JSON payload struct
func (m *mlEngine) createJSONPayload(issue *spamcheck.Issue) *jsonPayload {
	currentUser := issue.GetUser()
	user := user{
		Emails:    currentUser.GetEmails(),
		Org:       currentUser.GetOrg(),
		Username:  currentUser.GetUsername(),
		CreatedAt: currentUser.GetCreatedAt(),
	}

	entityPayload := entity{
		ID:            1,
		User:          user,
		Title:         issue.GetTitle(),
		Description:   issue.GetDescription(),
		CreatedAt:     issue.GetCreatedAt(),
		UpdatedAt:     issue.GetUpdatedAt(),
		Action:        issue.GetAction(),
		UserInProject: issue.GetUserInProject(),
	}

	jsonPayload := jsonPayload{
		Entity: entityPayload,
	}
	return &jsonPayload
}

func (m *mlEngine) sendJSONPayload(jp *jsonPayload) float32 {
	// serializing the struct into JSON
	jsonValue, err := json.Marshal(jp)
	if err != nil {
		log.WithFields(log.Fields{
			"metric": "spamcheck_tflite_errors",
		}).WithError(err).Error("Error marshalling issue to JSON")
		return 0
	}

	data, err := m.preprocessData(jsonValue)

	if err != nil {
		log.WithFields(log.Fields{
			"metric": "spamcheck_tflite_errors",
		}).WithError(err).Error("Error preprocessing data")
		return 0
	}

	path, err := os.Getwd()

	if err != nil {
		log.WithFields(log.Fields{
			"metric": "spamcheck_tflite_errors",
		}).WithError(err).Error("Error getting working directory")
		panic(err)
	}

	modelPath := m.cfg.ModelAttributes.ModelPath
	if modelPath == "" {
		modelPath = filepath.Join(path, "app", "mlengine", "model.tflite")
	}

	name := C.CString(modelPath)
	model := C.TfLiteModelCreateFromFile(name)
	if model == nil {
		log.WithFields(log.Fields{
			"metric": "spamcheck_tflite_errors",
		}).Error("Error loading model")
		return 0
	}

	options := C.TfLiteInterpreterOptionsCreate()
	if options == nil {
		log.WithFields(log.Fields{
			"metric": "spamcheck_tflite_errors",
		}).Error("Failed to create options")
		return 0
	}

	runner := C.TfLiteInterpreterCreate(model, options)
	if runner == nil {
		log.WithFields(log.Fields{
			"metric": "spamcheck_tflite_errors",
		}).Error("Failed to create interpreter")
		return 0
	}

	C.TfLiteInterpreterAllocateTensors(runner)
	var verdict float32

	for i := 0; i < len(data); i++ {
		input, err := runner.GetInputTensor(0)
		if err != nil {
			log.WithFields(log.Fields{
				"metric": "spamcheck_tflite_errors",
			}).WithError(err).Error("Error getting input tensor", err)
		}
		status := input.CopyFromBuffer(data[i])
		if status != C.kTfLiteOk {
			size := C.TfLiteTensorByteSize(input.tensor)
			log.WithFields(log.Fields{
				"metric":      "spamcheck_tflite_errors",
				"buffer_size": size,
				"status":      status,
			}).Error("Error copying from buffer")
			return 0
		}

		if C.TfLiteInterpreterInvoke(runner) != C.kTfLiteOk {
			log.WithFields(log.Fields{
				"metric": "spamcheck_tflite_errors",
			}).Error("Error - failed to invoke runner")
			return 0
		}
		output := runner.GetOutputTensor(0)
		if output == nil {
			log.WithFields(log.Fields{
				"metric": "spamcheck_tflite_errors",
			}).Error("Error - output tensor is empty")
			return 0
		}

		verdict = output.Float32s()[0]
		log.WithFields(log.Fields{
			"verdict": verdict,
			"metric":  "spamcheck_tflite_verdicts",
		}).Info("Verdict returned by TFLite")
	}
	C.TfLiteInterpreterDelete(runner)
	C.TfLiteModelDelete(model)
	return verdict
}

func (i *C.TfLiteInterpreter) GetInputTensor(index int) (*Tensor, error) {
	if i != nil {
		t := C.TfLiteInterpreterGetInputTensor(i, C.int32_t(index))
		return &Tensor{tensor: t}, nil
	}
	return nil, fmt.Errorf("err with input tensor")
}

func (i *C.TfLiteInterpreter) GetOutputTensor(index int) *Tensor {
	t := C.TfLiteInterpreterGetOutputTensor(i, C.int32_t(index))
	if t == nil {
		return nil
	}
	return &Tensor{tensor: t}
}

// CopyFromBuffer write buffer to the tensor.
func (t *Tensor) CopyFromBuffer(b interface{}) C.TfLiteStatus {
	return C.TfLiteTensorCopyFromBuffer(t.tensor, unsafe.Pointer(reflect.ValueOf(b).Pointer()), C.size_t(t.ByteSize()))
}

func (t *Tensor) Float32s() []float32 {
	if t.Type() != TfLiteFloat32 {
		return nil
	}
	ptr := C.TfLiteTensorData(t.tensor)
	if ptr == nil {
		return nil
	}
	n := t.ByteSize() / 4
	return (*((*[1<<29 - 1]float32)(ptr)))[:n]
}
