package controllers

import (
	"context"
	"testing"

	"github.com/golang/protobuf/ptypes"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/mock"
	spamcheck "gitlab.com/gitlab-org/spamcheck/api/v1"
	config "gitlab.com/gitlab-org/spamcheck/app/config"
)

var testGRPC = &config.GRPC{
	Port: "5050",
}

var testREST = &config.REST{
	ExternalPort: "5051",
}

var testLogger = &config.Logger{
	Level:  "info",
	Output: "json",
	Format: "stderr",
}

var testMonitor = &config.Monitor{
	Address: "5053",
}

var testExtraAttributes = &config.ExtraAttributes{
	MonitorMode: "false",
}

var testFilter = &config.Filter{
	AllowList: map[string]string{"14": "spamtest/hello"},
	DenyList:  map[string]string{},
}

var testPreprocessor = &config.Preprocessor{
	SocketPath: "/foobar/preprocessor.sock",
}

var testModelAttributes = &config.ModelAttributes{
	ModelPath: "/foobar/model.tflite",
}

var testConfig = &config.Config{
	*testGRPC,
	*testREST,
	*testLogger,
	*testMonitor,
	*testExtraAttributes,
	*testFilter,
	*testPreprocessor,
	*testModelAttributes,
}

type MockMlEngine struct {
	mock.Mock
}

func (m *MockMlEngine) GetSpamValue(issue *spamcheck.Issue) float32 {
	args := m.Called(issue)
	return args.Get(0).(float32)
}

func TestCheckForSpamIssue(t *testing.T) {
	var err error
	mockMlEngine := new(MockMlEngine)

	if err != nil {
		t.Fatal(err)
	}

	gitlab_employee := spamcheck.User{
		Username: "Spaceman Spiff",
		Emails: []*spamcheck.User_Email{
			{
				Email:    "spaceman.spiff@gitlab.com",
				Verified: true,
			},
		},
		Org:       "gitlab",
		CreatedAt: ptypes.TimestampNow(),
	}

	user := spamcheck.User{
		Username: "Mr. Stupendous",
		Emails: []*spamcheck.User_Email{
			{
				Email:    "mr.stupendous@email.com",
				Verified: true,
			},
		},
		Org:       "gitlab",
		CreatedAt: ptypes.TimestampNow(),
	}

	project := spamcheck.Project{
		ProjectId:   14,
		ProjectPath: "spamtest/hello",
	}

	project1 := spamcheck.Project{
		ProjectId:   21,
		ProjectPath: "testy/test",
	}

	t.Run("testing with non-spam Issue", func(t *testing.T) {
		controller := CreateSpamCheckController(testConfig, mockMlEngine)

		issue := spamcheck.Issue{
			User:          &user,
			Title:         "Test title",
			Description:   "test description",
			UserInProject: false,
			CreatedAt:     ptypes.TimestampNow(),
			Project:       &project,
		}
		mockMlEngine.On("GetSpamValue", &issue).Return(float32(0.01))

		got, err := controller.CheckForSpamIssue(context.Background(), &issue)
		if err != nil {
			assert.Failf(t, "Unexpected error", "Error when checking for spam %s", err)
		}
		want := spamcheck.SpamVerdict_ALLOW

		assert.Equal(t, got.Verdict, want, "Got %s want %s")
	})

	t.Run("testing with spam Issue", func(t *testing.T) {
		controller := CreateSpamCheckController(testConfig, mockMlEngine)

		issue := spamcheck.Issue{
			User:          &user,
			Title:         "Test title",
			Description:   "test description",
			UserInProject: false,
			CreatedAt:     ptypes.TimestampNow(),
			Project:       &project,
		}

		mockMlEngine.On("GetSpamValue", &issue).Return(float32(0.99))
		got, err := controller.CheckForSpamIssue(context.Background(), &issue)
		if err != nil {
			assert.Failf(t, "Unexpected error", "Error when checking for spam %s", err)
		}
		/*
		TODO: rollback whenever https://gitlab.com/gitlab-com/gl-security/engineering-and-research/automation-team/spam/spamcheck/-/issues/191, https://gitlab.com/gitlab-com/gl-security/engineering-and-research/automation-team/spam/spamcheck/-/issues/193, https://gitlab.com/gitlab-com/gl-security/engineering-and-research/automation-team/spam/spamcheck/-/issues/188 are addressed
		want := spamcheck.SpamVerdict_BLOCK
		*/
		want := spamcheck.SpamVerdict_CONDITIONAL_ALLOW

		assert.Equal(t, got.Verdict, want, "Got %s want %s")
	})

	t.Run("testing with likely spam Issue", func(t *testing.T) {
		controller := CreateSpamCheckController(testConfig, mockMlEngine)

		issue := spamcheck.Issue{
			User:          &user,
			Title:         "Test title",
			Description:   "test description",
			UserInProject: false,
			CreatedAt:     ptypes.TimestampNow(),
			Project:       &project,
		}

		mockMlEngine.On("GetSpamValue", &issue).Return(float32(0.49))
		got, err := controller.CheckForSpamIssue(context.Background(), &issue)
		if err != nil {
			assert.Failf(t, "Unexpected error", "Error when checking for spam %s", err)
		}
		want := spamcheck.SpamVerdict_CONDITIONAL_ALLOW

		assert.Equal(t, got.Verdict, want, "Got %s want %s")
	})

	t.Run("testing with highly likely spam Issue", func(t *testing.T) {
		controller := CreateSpamCheckController(testConfig, mockMlEngine)

		issue := spamcheck.Issue{
			User:          &user,
			Title:         "Test title",
			Description:   "test description",
			UserInProject: false,
			CreatedAt:     ptypes.TimestampNow(),
			Project:       &project,
		}

		mockMlEngine.On("GetSpamValue", &issue).Return(float32(0.59))
		got, err := controller.CheckForSpamIssue(context.Background(), &issue)
		if err != nil {
			assert.Failf(t, "Unexpected error", "Error when checking for spam %s", err)
		}
		/*
		TODO: rollback whenever https://gitlab.com/gitlab-com/gl-security/engineering-and-research/automation-team/spam/spamcheck/-/issues/191, https://gitlab.com/gitlab-com/gl-security/engineering-and-research/automation-team/spam/spamcheck/-/issues/193, https://gitlab.com/gitlab-com/gl-security/engineering-and-research/automation-team/spam/spamcheck/-/issues/188 are addressed
		want := spamcheck.SpamVerdict_DISALLOW
		*/
		want := spamcheck.SpamVerdict_CONDITIONAL_ALLOW


		assert.Equal(t, got.Verdict, want, "Got %s want %s")
	})

	t.Run("testing with populated AL, empty DL and matching AL", func(t *testing.T) {
		controller := CreateSpamCheckController(testConfig, mockMlEngine)

		issue := spamcheck.Issue{
			User:          &user,
			Title:         "Test title",
			Description:   "test description",
			UserInProject: false,
			CreatedAt:     ptypes.TimestampNow(),
			Project:       &project,
		}

		mockMlEngine.On("GetSpamValue", &issue).Return(float32(0.99))
		got, err := controller.CheckForSpamIssue(context.Background(), &issue)
		if err != nil {
			assert.Failf(t, "Unexpected error", "Error when checking for spam %s", err)
		}
		
		/*
		TODO: rollback whenever https://gitlab.com/gitlab-com/gl-security/engineering-and-research/automation-team/spam/spamcheck/-/issues/191, https://gitlab.com/gitlab-com/gl-security/engineering-and-research/automation-team/spam/spamcheck/-/issues/193, https://gitlab.com/gitlab-com/gl-security/engineering-and-research/automation-team/spam/spamcheck/-/issues/188 are addressed
		want := spamcheck.SpamVerdict_BLOCK
		*/
		want := spamcheck.SpamVerdict_CONDITIONAL_ALLOW

		assert.Equal(t, got.Verdict, want, "Got %s want %s")
	})

	t.Run("testing with populated AL, empty DL and not matching AL", func(t *testing.T) {
		controller := CreateSpamCheckController(testConfig, mockMlEngine)

		issue := spamcheck.Issue{
			User:          &user,
			Title:         "Test title",
			Description:   "test description",
			UserInProject: false,
			CreatedAt:     ptypes.TimestampNow(),
			Project:       &project1,
		}

		got, err := controller.CheckForSpamIssue(context.Background(), &issue)
		if err != nil {
			assert.Failf(t, "Unexpected error", "Error when checking for spam %s", err)
		}
		want := spamcheck.SpamVerdict_NOOP

		assert.Equal(t, got.Verdict, want, "Got %s want %s")
	})

	t.Run("testing with empty AL, populated DL and matching DL", func(t *testing.T) {
		var testFilter1 = &config.Filter{
			AllowList: map[string]string{},
			DenyList:  map[string]string{"21": "testy/test"},
		}

		var testConfig1 = &config.Config{
			*testGRPC,
			*testREST,
			*testLogger,
			*testMonitor,
			*testExtraAttributes,
			*testFilter1,
			*testPreprocessor,
			*testModelAttributes,
		}

		controller := CreateSpamCheckController(testConfig1, mockMlEngine)

		issue := spamcheck.Issue{
			User:          &user,
			Title:         "Test title",
			Description:   "test description",
			UserInProject: false,
			CreatedAt:     ptypes.TimestampNow(),
			Project:       &project1,
		}

		got, err := controller.CheckForSpamIssue(context.Background(), &issue)
		if err != nil {
			assert.Failf(t, "Unexpected error", "Error when checking for spam %s", err)
		}
		want := spamcheck.SpamVerdict_NOOP

		assert.Equal(t, got.Verdict, want, "Got %s want %s")
	})

	t.Run("testing with empty AL, populated DL and not matching DL", func(t *testing.T) {
		var testFilter1 = &config.Filter{
			AllowList: map[string]string{},
			DenyList:  map[string]string{"21": "testy/test"},
		}

		var testConfig1 = &config.Config{
			*testGRPC,
			*testREST,
			*testLogger,
			*testMonitor,
			*testExtraAttributes,
			*testFilter1,
			*testPreprocessor,
			*testModelAttributes,
		}

		issue := spamcheck.Issue{
			User:          &user,
			Title:         "Test title",
			Description:   "test description",
			UserInProject: false,
			CreatedAt:     ptypes.TimestampNow(),
			Project:       &project,
		}
		mockMlEngine.On("GetSpamValue", &issue).Return(float32(0.99))
		controller := CreateSpamCheckController(testConfig1, mockMlEngine)

		got, err := controller.CheckForSpamIssue(context.Background(), &issue)
		if err != nil {
			assert.Failf(t, "Unexpected error", "Error when checking for spam %s", err)
		}
		/*
		TODO: rollback whenever https://gitlab.com/gitlab-com/gl-security/engineering-and-research/automation-team/spam/spamcheck/-/issues/191, https://gitlab.com/gitlab-com/gl-security/engineering-and-research/automation-team/spam/spamcheck/-/issues/193, https://gitlab.com/gitlab-com/gl-security/engineering-and-research/automation-team/spam/spamcheck/-/issues/188 are addressed
		want := spamcheck.SpamVerdict_BLOCK
		*/
		want := spamcheck.SpamVerdict_CONDITIONAL_ALLOW

		assert.Equal(t, got.Verdict, want, "Got %s want %s")
	})

	t.Run("testing with empty AL, empty DL", func(t *testing.T) {
		var testFilter1 = &config.Filter{
			AllowList: map[string]string{},
			DenyList:  map[string]string{},
		}

		var testConfig1 = &config.Config{
			*testGRPC,
			*testREST,
			*testLogger,
			*testMonitor,
			*testExtraAttributes,
			*testFilter1,
			*testPreprocessor,
			*testModelAttributes,
		}

		issue := spamcheck.Issue{
			User:          &user,
			Title:         "Test title",
			Description:   "test description",
			UserInProject: false,
			CreatedAt:     ptypes.TimestampNow(),
			Project:       &project1,
		}
		mockMlEngine.On("GetSpamValue", &issue).Return(float32(0.99))
		controller := CreateSpamCheckController(testConfig1, mockMlEngine)

		got, err := controller.CheckForSpamIssue(context.Background(), &issue)
		if err != nil {
			assert.Failf(t, "Unexpected error", "Error when checking for spam %s", err)
		}
		/*
		TODO: rollback whenever https://gitlab.com/gitlab-com/gl-security/engineering-and-research/automation-team/spam/spamcheck/-/issues/191, https://gitlab.com/gitlab-com/gl-security/engineering-and-research/automation-team/spam/spamcheck/-/issues/193, https://gitlab.com/gitlab-com/gl-security/engineering-and-research/automation-team/spam/spamcheck/-/issues/188 are addressed
		want := spamcheck.SpamVerdict_BLOCK
		*/
		want := spamcheck.SpamVerdict_CONDITIONAL_ALLOW

		assert.Equal(t, got.Verdict, want, "Got %s want %s")
	})

	/* TODO These 2 tests should move to mlengine/mlengine_test.go when we create it
	t.Run("testing with error Posting to MlEngine", func(t *testing.T) {
		controller := CreateSpamCheckController(testConfig, mockMlEngine)

		issue := spamcheck.Issue{
			User:          &user,
			Title:         "Test title",
			Description:   "test description",
			UserInProject: true,
			CreatedAt:     ptypes.TimestampNow(),
			Project:       &project,
		}

		mockMlEngine.On("GetSpamValue", &issue).Return(float32(0.99))
		got, err := controller.CheckForSpamIssue(context.Background(), &issue)
		if err != nil {
			assert.Failf(t, "Unexpected error", "Error when checking for spam %s", err)
		}

		expected := spamcheck.SpamVerdict_NOOP

		assert.Equal(t, got.Verdict, expected, "Got %s want %s")
	})

		t.Run("testing returns error with verdict when Posting to MlEngine fails", func(t *testing.T) {
			controller := CreateSpamCheckController(testConfig, mockMlEngine)

			issue := spamcheck.Issue{
				User:          &user,
				Title:         "Test title",
				Description:   "test description",
				UserInProject: true,
				CreatedAt:     ptypes.TimestampNow(),
				Project:       &project,
			}

			got, err := controller.CheckForSpamIssue(context.Background(), &issue)
			if err != nil {
				assert.Failf(t, "Unexpected error", "Error when checking for spam %s", err)
			}

			expected := "unexpected end of JSON input"

			assert.Equal(t, got.Error, expected, "Got %s want %s")
		})
	*/

	t.Run("testing returns ALLOW when User is a GitLab employee", func(t *testing.T) {
		controller := CreateSpamCheckController(testConfig, mockMlEngine)

		issue := spamcheck.Issue{
			User:          &gitlab_employee,
			Title:         "Test title",
			Description:   "test description",
			UserInProject: false,
			CreatedAt:     ptypes.TimestampNow(),
			Project:       &project,
		}

		mockMlEngine.On("GetSpamValue", &issue).Return(float32(0.99))
		got, err := controller.CheckForSpamIssue(context.Background(), &issue)
		if err != nil {
			assert.Failf(t, "Unexpected error", "Error when checking for spam %s", err)
		}

		expected := spamcheck.SpamVerdict_ALLOW

		assert.Equal(t, got.Verdict, expected, "Got %s want %s")
	})

	t.Run("testing returns ALLOW when User is a project member", func(t *testing.T) {
		controller := CreateSpamCheckController(testConfig, mockMlEngine)

		issue := spamcheck.Issue{
			User:          &user,
			Title:         "Test title",
			Description:   "test description",
			UserInProject: true,
			CreatedAt:     ptypes.TimestampNow(),
			Project:       &project,
		}

		mockMlEngine.On("GetSpamValue", &issue).Return(float32(0.99))
		got, err := controller.CheckForSpamIssue(context.Background(), &issue)
		if err != nil {
			assert.Failf(t, "Unexpected error", "Error when checking for spam %s", err)
		}

		expected := spamcheck.SpamVerdict_ALLOW

		assert.Equal(t, got.Verdict, expected, "Got %s want %s")
	})
}
