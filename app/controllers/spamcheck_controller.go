package controllers

import (
	"context"
	"strconv"
	"strings"

	"gitlab.com/gitlab-org/labkit/correlation"
	"gitlab.com/gitlab-org/labkit/log"
	spamcheck "gitlab.com/gitlab-org/spamcheck/api/v1"
	config "gitlab.com/gitlab-org/spamcheck/app/config"
	mlengine "gitlab.com/gitlab-org/spamcheck/app/mlengine"
)

// SpamCheckController Is the core of the business logic that controls how objects are checked for spam
type SpamCheckController interface {
	spamcheck.SpamcheckServiceServer
}

type spamCheckController struct {
	cfg       *config.Config
	mlEngine  mlengine.MlEngine

	//Needed for GRPC V2, not ours.
	spamcheck.UnimplementedSpamcheckServiceServer
}

// CreateSpamCheckController is the "constructor" SpamCheckController
func CreateSpamCheckController(cfg *config.Config, mlEngine mlengine.MlEngine) SpamCheckController {
	return &spamCheckController{
		cfg:       cfg,
		mlEngine: mlEngine,
	}
}

func (s *spamCheckController) IsProjectAllowed(project *spamcheck.Project) bool {
	var result bool

	// checks allowlist first by default
	if len(s.cfg.Filter.AllowList) != 0 {
		if _, exists := s.cfg.Filter.AllowList[strconv.Itoa(int(project.GetProjectId()))]; exists {
			// explicitly allowed project
			result = true
		} else {
			// id doesn't match any key in allowlist
			result = false
		}
	} else if len(s.cfg.Filter.DenyList) != 0 {
		if _, exists := s.cfg.Filter.DenyList[strconv.Itoa(int(project.GetProjectId()))]; exists {
			// explicitly denied project
			result = false
		} else {
			// id doesn't match any key in denylist
			result = true
		}
	} else {
		// if both lists are empty, allow everything
		result = true
	}

	return result
}

// CheckForSpamIssue does the actual checking for spam
func (s *spamCheckController) CheckForSpamIssue(ctx context.Context, issue *spamcheck.Issue) (*spamcheck.SpamVerdict, error) {
	var result spamcheck.SpamVerdict

	attribs := make(map[string]string)
	attribs["monitorMode"] = s.cfg.ExtraAttributes.MonitorMode
	project := issue.GetProject()
	correlationID := correlation.ExtractFromContext(ctx)

	isAllowedProject := s.IsProjectAllowed(project)
	if isAllowedProject == false {
		result = spamcheck.SpamVerdict{
			Verdict:         spamcheck.SpamVerdict_NOOP,
			ExtraAttributes: attribs,
		}
		log.WithFields(log.Fields{
			"metric":         "spamcheck_verdicts",
			"monitor_mode":   s.cfg.ExtraAttributes.MonitorMode,
			"project_path":   project.GetProjectPath(),
			"verdict":        result.GetVerdict().String(),
			"correlation_id": correlationID,
		}).Info("Verdict for disallowed project")

		return &result, nil
	}

	verdictValue := s.mlEngine.GetSpamValue(issue)

	isEmailAllowlisted := s.isEmailAllowlisted(issue.GetUser())
	if isEmailAllowlisted || issue.GetUserInProject() {
		verdictValue = 0.0
	}

	if verdictValue >= 0.9 {
		result = spamcheck.SpamVerdict{
			Verdict:         spamcheck.SpamVerdict_BLOCK,
			ExtraAttributes: attribs,
		}
	} else if verdictValue > 0.5 && verdictValue < 0.9 {
		result = spamcheck.SpamVerdict{
			Verdict:         spamcheck.SpamVerdict_DISALLOW,
			ExtraAttributes: attribs,
		}
	} else if verdictValue >= 0.4 && verdictValue <= 0.5 {
		result = spamcheck.SpamVerdict{
			Verdict:         spamcheck.SpamVerdict_CONDITIONAL_ALLOW,
			ExtraAttributes: attribs,
		}
	} else {
		result = spamcheck.SpamVerdict{
			Verdict:         spamcheck.SpamVerdict_ALLOW,
			ExtraAttributes: attribs,
		}
	}

	log.WithFields(log.Fields{
		"email_allowlisted": isEmailAllowlisted,
		"user_in_project":   issue.GetUserInProject(),
		"metric":            "spamcheck_verdicts",
		"monitor_mode":      s.cfg.ExtraAttributes.MonitorMode,
		"project_path":      project.GetProjectPath(),
		"verdict":           result.GetVerdict().String(),
		"correlation_id":    correlationID,
	}).Info("Verdict calculated")

        switch result.Verdict {
                case
                        spamcheck.SpamVerdict_DISALLOW,
                        spamcheck.SpamVerdict_BLOCK:
                        result.Verdict = spamcheck.SpamVerdict_CONDITIONAL_ALLOW
        }

	return &result, nil
}

func (s *spamCheckController) isEmailAllowlisted(user *spamcheck.User) bool {
	for _, email := range user.Emails {
		if email.Verified && strings.HasSuffix(email.Email, "@gitlab.com") {
			return true
		}
	}
	return false
}
