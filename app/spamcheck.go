package app

import (
	"context"
	"flag"

	"gitlab.com/gitlab-org/labkit/log"
	config "gitlab.com/gitlab-org/spamcheck/app/config"
	controller "gitlab.com/gitlab-org/spamcheck/app/controllers"
	logging "gitlab.com/gitlab-org/spamcheck/app/logging"
	mlengine "gitlab.com/gitlab-org/spamcheck/app/mlengine"
	monitoring "gitlab.com/gitlab-org/spamcheck/app/monitoring"
	"gitlab.com/gitlab-org/spamcheck/app/protocol/grpc"
	"gitlab.com/gitlab-org/spamcheck/app/protocol/rest"
	services "gitlab.com/gitlab-org/spamcheck/app/services"
	tracing "gitlab.com/gitlab-org/spamcheck/app/tracing"
	"gitlab.com/gitlab-org/spamcheck/app/validations"
)

// SpamCheck is the main application entrypoint
type SpamCheck struct {
}

// Run runs gRPC server and HTTP gateway
func (s *SpamCheck) Run() error {
	ctx := context.Background()

	configPath := flag.String("config", "config/config.toml", "Specify the path to the config.toml file")

	flag.Parse()

	cfg, err := config.RunConfigWithConfigFile(*configPath)
	if err != nil {
		log.WithError(err).Error("Run(): Error loading configuration")
		return err
	}

	// Initialize LabKit services
	setupLabKit(cfg)

	// Set up controller, service, and validations
	mlEngine := mlengine.CreateMlEngine(cfg)
	controller := controller.CreateSpamCheckController(cfg, mlEngine)
	validations := validations.CreateSpamCheckValidations()

	v1API := services.CreateSpamCheckService(controller, validations)

	if len(cfg.REST.ExternalPort) > 0 {
		// run HTTP gateway
		go func() {
			_ = rest.RunServer(ctx, cfg.GRPC.Port, cfg.REST.ExternalPort)
		}()
	}

	return grpc.RunServer(ctx, v1API, cfg.GRPC.Port)
}

// Load LabKit configurations/setup
func setupLabKit(cfg *config.Config) {
	err := logging.LoadLogging(cfg)
	if err != nil {
		log.WithError(err).Panic("Error loading logging")
	}

	err = monitoring.LoadMonitoring(cfg)
	if err != nil {
		log.WithError(err).Panic("Monitoring stopped")
	}

	tracing.LoadTracing()
}
