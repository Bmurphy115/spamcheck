# Upgrade strategy
# Version number is in the form Major.Minor.Patch
# Increment the patch version when making backard compatible bug fixes
# Increment the minor version when adding functionality (that is backward compatible)
# Increment the major version when making incompatible changes
module SpamCheck
    VERSION = '0.1.0'
end
